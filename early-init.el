;; This file is loaded before package.el is initialized, and before
;; the first graphical frame is initialized, by Emacs 27 (but not by
;; any previous version of Emacs).
;;
;; If the early init-file is available, we actually execute our entire
;; init process within it, by just loading the regular init-file.
;; (That file takes care of making sure it is only loaded once.)

;; Disable ugly bars
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; Speed up initialization
(setq package-enable-at-startup nil) 	; disable package.el, use straight.el instead
(setq load-prefer-newer t)
(setq message-log-max 16384)
(setq frame-inhibit-implied-resize t)
(setq gc-cons-threshold most-positive-fixnum)
(setq gc-cons-percentage 0.9)

(defun z-reset-gc-cons ()
  "Return the garbage collection threshold to default values."
  (setq gc-cons-threshold
	(car (get 'gc-cons-threshold 'standard-value))
	gc-cons-percentage
	(car (get 'gc-cons-percentage 'standard-value))))

(add-hook 'after-init-hook #'z-reset-gc-cons)
