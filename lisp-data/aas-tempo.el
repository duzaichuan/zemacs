;;; aas-tempo.el --- A bundle of as-you-type LaTeX and unicode snippets -*- lexical-binding: t; -*-
(require 'aas)
(require 'tempel)

(aas-set-snippets 'telega-chat-mode
  ;; expand unconditionally
  ";smile" "☺"
  ";nerd" "🤓"
  ";kiss" "😚"
  ";cry" "😢"
  ";thx" "🙏"
  ";horror" "😱"
  ";xcry" "😂"
  ";tongue" "😜"
  ";thumb" "👍"
  ";ok" "👌"
  ";clap" "👏"
  ";callme" "🤙")

(aas-set-snippets 'julia-mode
  ";a" "α"
  ";b" "β"
  ";g" "γ"
  ";d" "δ"
  ";e" "ε"
  ";z" "ζ"
  ";h" "η"
  ";q" "θ"
  ";i" "ι"
  ";k" "κ"
  ";l" "λ"
  ";m" "μ"
  ";n" "ν"
  ";x" "ξ"
  ";p" "π"
  ";r" "ρ"
  ";s" "σ"
  ";t" "τ"
  ";v" "υ"
  ";f" "φ"
  ";c" "χ"
  ";y" "ψ"
  ";o" "ω"
  ";G" "Γ"
  ";D" "Δ"
  ";Q" "Θ"
  ";L" "Λ"
  ";X" "Ξ"
  ";P" "Π"
  ";S" "Σ"
  ";F" "Φ"
  ";Y" "Ψ"
  ";O" "Ω")

(defun laas-insert-script (s)
  "Add a subscript with a text of S (string).

Rely on `aas-transient-snippet-condition-result' to contain the
result of `aas-auto-script-condition' which gives the info
whether to extend an existing subscript (e.g a_1 -> a_{1n}) or
insert a new subscript (e.g a -> a_1)."
  (interactive (list (this-command-keys)))
  (pcase aas-transient-snippet-condition-result
    ;; new subscript after a letter
    ('one-sub
     (insert "_" s))
    ;; continuing a digit sub/superscript
    ('extended-sub
     (backward-char)
     (insert "{")
     (forward-char)
     (insert s "}"))))

(defun laas-org-mathp ()
  "Determine whether the point is within a LaTeX fragment or environment."
  (or (org-inside-LaTeX-fragment-p)
      (eq (org-element-type (org-element-at-point)) 'latex-environment)))

(defun laas-auto-script-condition ()
  "Condition used for auto-sub/superscript snippets."
  (cond ((or (bobp) (= (1- (point)) (point-min)))
         nil)
        ((and (or (= (char-before (1- (point))) ?_)
                  (= (char-before (1- (point))) ?^))
              (/= (char-before) ?{))
         'extended-sub)
        ((and
          ;; Before is some indexable char
          (or (<= ?a (char-before) ?z)
              (<= ?A (char-before) ?Z))
          ;; Before that is not
          (not (or (<= ?a (char-before (1- (point))) ?z)
                   (<= ?A (char-before (1- (point))) ?Z)))
          ;; Inside math
          (laas-org-mathp))
         'one-sub)))

(aas-set-snippets 'org-mode
  ";4" (lambda () (interactive)
	 (tempel-insert '("\\(" p "\\)")))
  ";$" (lambda () (interactive)
	 (tempel-insert '("\\[" n r n "\\]" n))))

(aas-set-snippets 'org-mode
  :cond #'laas-auto-script-condition
  "ii" #'laas-insert-script
  "ip1" "_{i+1}"
  "im1" "_{i-1}"
  "jj" #'laas-insert-script
  "jp1" "_{j+1}"
  "jm1" "_{j-1}"
  "nn" #'laas-insert-script
  "np1" "_{n+1}"
  "nm1" "_{n-1}"
  "kk" #'laas-insert-script
  "kp1" "_{k+1}"
  "km1" "_{k-1}"
  "0" #'laas-insert-script
  "1" #'laas-insert-script
  "2" #'laas-insert-script
  "3" #'laas-insert-script
  "4" #'laas-insert-script
  "5" #'laas-insert-script
  "6" #'laas-insert-script
  "7" #'laas-insert-script
  "8" #'laas-insert-script
  "9" #'laas-insert-script)

(aas-set-snippets 'org-mode
  ;; set condition!
  :cond #'laas-org-mathp ; expand only while in math
  "!="    "\\neq"
  "!>"    "\\mapsto"
  "~="    "\\approx"
  "~~"    "\\sim"
  "<<"    "\\ll"
  "<="    "\\leq"
  ">="    "\\geq"
  ">>"    "\\gg"
  "//" 	(lambda () (interactive)
	  (tempel-insert '("\\frac{" p "}{" p "}")))
  ";a"  "\\alpha"
  ";A"  "\\forall"        ";;A" "\\aleph"
  ";b"  "\\beta"
  ";;;c" "\\cos"
  ";;;C" "\\arccos"
  ";d"  "\\delta"         ";;d" "\\partial"
  ";D"  "\\Delta"         ";;D" "\\nabla"
  ";e"  "\\epsilon"       ";;e" "\\varepsilon"   ";;;e" "\\exp"
  ";E"  "\\exists"                               ";;;E" "\\ln"
  ";f"  "\\phi"           ";;f" "\\varphi"
  ";F"  "\\Phi"
  ";g"  "\\gamma"                                ";;;g" "\\lg"
  ";G"  "\\Gamma"                                ";;;G" "10^{?}"
  ";h"  "\\eta"           ";;h" "\\hbar"
  ";i"  "\\in"            ";;i" "\\imath"
  ";I"  "\\iota"          ";;I" "\\Im"
  ";;j" "\\jmath"
  ";k"  "\\kappa"
  ";l"  "\\lambda"        ";;l" "\\ell"          ";;;l" "\\log"
  ";L"  "\\Lambda"
  ";m"  "\\mu"
  ";n"  "\\nu"                                   ";;;n" "\\ln"
  ";N"  "\\nabla"                                ";;;N" "\\exp"
  ";o"  "\\omega"
  ";O"  "\\Omega"         ";;O" "\\mho"
  ";p"  "\\pi"            ";;p" "\\varpi"
  ";P"  "\\Pi"
  ";q"  "\\theta"         ";;q" "\\vartheta"
  ";Q"  "\\Theta"
  ";r"  "\\rho"           ";;r" "\\varrho"
  ";;R" "\\Re"
  ";s"  "\\sigma"         ";;s" "\\varsigma"    ";;;s" "\\sin"
  ";S"  "\\Sigma"                               ";;;S" "\\arcsin"
  ";t"  "\\tau"                                 ";;;t" "\\tan"
  ";;;T" "\\arctan"
  ";u"  "\\upsilon"
  ";U"  "\\Upsilon"
  ";v"  "\\vee"
  ";V"  "\\Phi"
  ";w"  "\\xi"
  ";W"  "\\Xi"
  ";x"  "\\chi"
  ";y"  "\\psi"
  ";Y"  "\\Psi"
  ";z"  "\\zeta"
  ";0"  "\\emptyset"
  ";8"  "\\infty"
  ";!"  "\\neg"
  ";^"  "\\uparrow"
  ";&"  "\\wedge"
  ";~"  "\\approx"        ";;~" "\\simeq"
  ";_"  "\\downarrow"
  ";+"  "\\cup"           ";;+" "\\oplus"
  ";-"  "\\leftrightarrow"";;-" "\\longleftrightarrow"
  ";*"  "\\times"
  ";/"  "\\not"
  ";|"  "\\mapsto"        ";;|" "\\longmapsto"
  ";\\" "\\setminus"
  ";="  "\\Leftrightarrow"";;=" "\\Longleftrightarrow"
  ";(" "\\langle"
  ";)" "\\rangle"
  ";[" "\\Leftarrow"     ";;[" "\\Longleftarrow"
  ";]" "\\Rightarrow"    ";;]" "\\Longrightarrow"
  ";{"  "\\subset"
  ";}"  "\\supset"
  ";<"  "\\leftarrow"    ";;<" "\\longleftarrow"  ";;;<" "\\min"
  ";>"  "\\rightarrow"   ";;>" "\\longrightarrow" ";;;>" "\\max"
  ";'"  "\\prime"
  ";."  "\\cdot")

(provide 'aas-tempo)
