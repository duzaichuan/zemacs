;;-*-coding: utf-8;-*-
(define-abbrev-table 'org-mode-abbrev-table
  '(
    ("ec" "economics" nil :count 3)
    ("ecm" "econometrics" nil :count 2)
    ("hts" "heterogeneous" nil :count 1)
    ("hty" "heterogeneity" nil :count 0)
   ))

