;; A self-use configuration---zemacs. -*- lexical-binding: t -*-

(setq straight-check-for-modifications 'live-with-find
      straight-vc-git-default-clone-depth 1)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(defvar straight-prefix-map
  (let ((map (make-sparse-keymap)))
    (define-key map "s" #'straight-use-package)
    (define-key map "c" #'straight-check-all)
    (define-key map "p" #'straight-pull-package)
    (define-key map "P" #'straight-pull-all)
    (define-key map "r" #'straight-rebuild-package)
    (define-key map "R" #'straight-rebuild-all)
    (define-key map "x" #'straight-remove-unused-repos)
    (define-key map "e" #'straight-prune-build)
    map)
  "Keymap for straight commands")
(global-set-key (kbd "C-, p") straight-prefix-map)

(defmacro summon! (package &rest body)
  "Set up PACKAGE from an Elisp archive with rest BODY.
PACKAGE is a quoted symbol, while BODY consists of balanced
expressions."
  (declare (indent 1))
  `(progn
     (straight-use-package ,package)
     ,@body))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                      ;;
;;                       EMACS BUILT-IN SETUP                           ;;
;;                                                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; saving previous sessions
(desktop-save-mode 1)
(setq desktop-path '("~/.emacs.d/lisp-data/"))
;; Default initial screen
(setq inhibit-startup-echo-area-message "zaichuan")
(setq inhibit-startup-message t)
(setq initial-major-mode 'fundamental-mode)
(setq initial-scratch-message nil)
;; Warnings
(setq warning-suppress-types '((defvaralias)))
;; nerd-fonts-meslo AUR
(set-face-attribute 'default nil :font "MesloLGMDZ Nerd Font Mono-11")
;; pacman -S wqy-microhei
(set-fontset-font t 'han      (font-spec :family "WenQuanYi Micro Hei"))
(set-fontset-font t 'cjk-misc (font-spec :family "WenQuanYi Micro Hei"))
;; ttf-symbola AUR
(set-fontset-font t 'unicode "Symbola" nil 'append)
;; also wrap CJK
(setq word-wrap-by-category t)
;; disable audio bell
(setq ring-bell-function 'ignore)
;; make native compile warnings silent
(setq native-comp-async-report-warnings-errors 'silent)
;; start the initial frame maximized
(add-to-list 'initial-frame-alist '(fullscreen . fullboth))
;; record changes in window configurations
(winner-mode 1)
(delete-selection-mode t)
(setq tab-always-indent 'complete)
(setq inhibit-startup-screen t)
(setq confirm-kill-emacs 'y-or-n-p)
;; y-or-n-p
(setq use-short-answers t)
;; cursor
(blink-cursor-mode 0)
;; visual line
(global-visual-line-mode 1)
(global-prettify-symbols-mode 1)
(savehist-mode 1)
(setq inhibit-compacting-font-caches t)
;; store all backup and autosave files in the current dir
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))
(auto-save-visited-mode 1)
;; Fill columns
(setq-default major-mode 'text-mode
	      fill-column 80)
(global-set-key (kbd "s-q") #'fill-paragraph)
(defun my/unfill-paragraph-or-region (&optional region)
  "Takes a multi-line paragraph and makes it into a single line of text."
  (interactive (progn (barf-if-buffer-read-only) '(t)))
  (let ((fill-column (point-max))
	;; This would override `fill-column' if it's an integer.
	(emacs-lisp-docstring-fill-column t))
    (fill-paragraph nil region)))
(define-key global-map (kbd "s-Q") #'my/unfill-paragraph-or-region)
;; secrets
(setq epg-gpg-program "gpg")
(setenv "GPG_AGENT_INFO" nil)
;; disable right-to-left display
(setq-default bidi-paragraph-direction 'left-to-right)
(setq bidi-inhibit-bpa t)
;; lisp-data/ load path
(add-to-list 'load-path "~/.emacs.d/lisp-data/")
(setq savehist-file "~/.emacs.d/lisp-data/history")
(setq nsm-settings-file "~/.emacs.d/lisp-data/network-security.data")
(setq tramp-persistency-file-name "~/.emacs.d/lisp-data/tramp")
(setq bookmark-file "~/.emacs.d/lisp-data/bookmarks")
(setq project-list-file "~/.emacs.d/lisp-data/projects")
(setq recentf-save-file "~/.emacs.d/lisp-data/recentf")
(setq abbrev-file-name "~/.emacs.d/lisp-data/emacs_abbrev.el")
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(setq recentf-max-saved-items 50)

;;;==============================================
;;;            Abbrevs and Expansion
;;;==============================================

;; turn on abbrev mode globally
(setq-default abbrev-mode t)
(setq abbrev-suggest t)
(setq save-abbrevs 'silently)
;; Dynamic abbreviation
(global-set-key (kbd "s-/") #'dabbrev-expand)
(setq dabbrev-abbrev-char-regexp "\\sw\\|\\s_")
(setq dabbrev-abbrev-skip-leading-regexp "[$*/=~']")
(setq dabbrev-upcase-means-case-search t)
;; Hippie expansion
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-complete-lisp-symbol-partially
        try-complete-lisp-symbol
        try-complete-file-name-partially
        try-complete-file-name))
(global-set-key (kbd "C-t") 'hippie-expand)

;;;==============================================
;;;                Parenthesis
;;;==============================================

;; smart parens
(setq show-paren-when-point-inside-paren t)
(electric-pair-mode 1)
;; match parens
(defun my/goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis. Else go to the
   opening parenthesis one level up."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1))
        (t
         (backward-char 1)
         (cond ((looking-at "\\s\)")
                (forward-char 1) (backward-list 1))
               (t
                (while (not (looking-at "\\s("))
                  (backward-char 1)
                  (cond ((looking-at "\\s\)")
                         (message "->> )")
                         (forward-char 1)
                         (backward-list 1)
                         (backward-char 1)))
                  ))))))
(global-set-key "%" #'my/goto-match-paren)
;; save minibuffer history

;;;==============================================
;;;
;;;==============================================

;; pulse lines like beacon
(defun my/pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))
(dolist (command '(recenter-top-bottom other-window))
  (advice-add command :after #'my/pulse-line))
;; Isearch
(setq isearch-lazy-count t)
(setq isearch-allow-motion t)
(setq isearch-allow-scroll 'unlimited)
(setq isearch-repeat-on-direction-change t)
(define-key isearch-mode-map (kbd "<backspace>") #'isearch-del-char)
;; recent files
(recentf-mode 1)
(global-set-key (kbd "C-, fr") #'recentf-open-files)
;; Dired
(setq dired-kill-when-opening-new-dired-buffer t)
(setq dired-recursive-deletes 'always)
(setq dired-recursive-copies 'always)
(setq dired-dwim-target t)
(global-set-key (kbd "C-x C-j") #'dired-jump)
;; Doc-view
(setq doc-view-resolution 200)
(setq doc-view-pdfdraw-program "mutool")
;; Octave
(add-to-list 'auto-mode-alist '("\\.m\\'" . octave-mode))
;; Display help buffer
(add-to-list 'display-buffer-alist
	     '("^\\*help"
	       (display-buffer-reuse-mode-window display-buffer-in-side-window)
	       (window-width . 0.25)
	       (side . left)
	       (slot . 0)))

;;;==============================================
;;;                KEYMAPS & ALIAS
;;;==============================================

;; global keys
(global-set-key (kbd "C-/") #'undo-only)
(global-set-key (kbd "C-?") #'undo-redo)
(global-set-key (kbd "s-n") (kbd "C-u 1 C-v"))
(global-set-key (kbd "s-p") (kbd "C-u 1 M-v"))
(global-set-key (kbd "<f2>") (lambda () (interactive) (find-file user-init-file)))
(global-set-key (kbd "<f4>") (lambda () (interactive) (find-file "~/.emacs.d/configs.org")))
(global-set-key (kbd "<f5>")  (lambda () (interactive) (find-file "~/org/bib/annotated_bib.org")))
(global-set-key (kbd "<s-f2>") #'ielm)
(global-set-key (kbd "<f8>") #'window-toggle-side-windows)
(global-set-key (kbd "s-[") #'previous-buffer)
(global-set-key (kbd "s-]") #'next-buffer)
(global-set-key (kbd "C-w") #'backward-kill-word)
(global-set-key (kbd "C-<backspace>") #'kill-region)
(global-set-key (kbd "s-k") #'kill-current-buffer)
(global-set-key (kbd "s-w") #'kill-ring-save)
(global-set-key (kbd "s-;") #'comment-line)
(global-set-key (kbd "C-, ff") #'find-file)
(global-set-key (kbd "C-, fb") #'bookmark-jump)
(global-set-key (kbd "C-, SPC") #'switch-to-buffer)
;; Alias
(defalias 'sh #'eshell)
(defalias 'wm #'woman)
(defalias 'ib #'ibuffer)
(defalias 'mk #'make-directory)
(defalias 'et #'emacs-init-time)
(defalias 'cs #'customize-group)
(defalias 'db #'toggle-debug-on-error)
(defalias 'fb #'flyspell-buffer)

;;;==============================================
;;;                 EWW Config
;;;==============================================

(global-set-key (kbd "C-, ii") #'eww)
(global-set-key (kbd "C-, io") #'eww-open-file)
(setq shr-width 110)
(setq shr-image-animate nil)
(setq shr-use-fonts nil)
(setq shr-bullet "• ")
(setq shr-folding-mode t)
(setq shr-use-colors nil)
(setq shr-max-image-proportion 0.5)
(setq eww-restore-desktop t)
(setq eww-header-line-format nil)
(setq url-cookie-trusted-urls '("^https://\\(www\\.\\)?aeaweb\\.org/.*"))
(setq eww-download-directory (expand-file-name "~/org/EWW"))
(setq eww-bookmarks-directory (locate-user-emacs-file "eww-bookmarks/"))
;; url handler
(defun my/browse-url-mpv (url &optional single)
  "Open video links with mpv"
  (start-process "mpv" nil "mpv" url)
  (message "Starting streaming..."))
(setq browse-url-handlers
      '(("https:\\/\\/www\\.youtu\\.*be." . my/browse-url-mpv)
	("github\\|reddit\\|twitter\\.com" . browse-url-chromium)
	("." . eww-browse-url)))
;; Rename eww buffers
(defun my/eww--rename-buffer ()
  "Rename EWW buffer using page title or URL.
To be used by `eww-after-render-hook'."
  (let ((name (if (eq "" (plist-get eww-data :title))
		  (plist-get eww-data :url)
		(plist-get eww-data :title))))
    (rename-buffer (format "*%s # eww*" name) t)))
(add-hook 'eww-after-render-hook #'my/eww--rename-buffer)
(advice-add 'eww-back-url :after #'my/eww--rename-buffer)
(advice-add 'eww-forward-url :after #'my/eww--rename-buffer)
;; Add standard Emacs bookmark support to Eww
(defun bookmark-eww--make ()
  "Make eww bookmark record."
  `((filename . ,(plist-get eww-data :url))
    (title . ,(plist-get eww-data :title))
    (time . ,(current-time-string))
    (handler . ,#'bookmark-eww-handler)
    (defaults . (,(concat
		   ;; url without the https and path
		   (replace-regexp-in-string
		    "/.*" ""
		    (replace-regexp-in-string
		     "\\`https?://" ""
		     (plist-get eww-data :url)))
		   " - "
		   ;; page title
		   (replace-regexp-in-string
		    "\\` +\\| +\\'" ""
		    (replace-regexp-in-string
		     "[\n\t\r ]+" " "
		     (plist-get eww-data :title))))))))
(defun bookmark-eww-handler (bm)
  "Handler for eww bookmarks."
  (eww-browse-url (alist-get 'filename bm)))
(defun bookmark-eww--setup ()
  "Setup eww bookmark integration."
  (setq-local bookmark-make-record-function #'bookmark-eww--make))
(add-hook 'eww-mode-hook #'bookmark-eww--setup)

;;;==============================================
;;;                    VC
;;;==============================================


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;                                                                      ;;
;;                       THIRD-PARTY PACKAGES                           ;;
;;                                                                      ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(summon! 'aas
  (add-hook 'org-mode-hook #'aas-activate-for-major-mode)
  (add-hook 'julia-mode-hook #'aas-activate-for-major-mode)
  (add-hook 'telega-chat-mode-hook #'aas-activate-for-major-mode)
  (with-eval-after-load "aas" (require 'aas-tempo)))

(summon! 'ace-link
  (global-set-key (kbd "s-l") #'ace-link)
  (with-eval-after-load "eww"
    (define-key eww-mode-map "f" #'ace-link)))

(summon! 'annotate
  (defalias 'an #'annotate-mode)
  (setq annotate-file "~/Projects/annotations"))

(summon! 'auctex
  (add-to-list 'auto-mode-alist '("\\.tex\\'" . TeX-latex-mode)))

(summon! 'avy
  (global-set-key (kbd "s-j") #'avy-goto-char-timer)
  (define-key isearch-mode-map (kbd "s-j") #'avy-isearch))

(summon! 'biblio
  (global-set-key (kbd "C-, br") #'biblio-crossref-lookup)
  (setq biblio-download-directory "~/org/bib/pdfs"))

(summon! 'cape
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (add-to-list 'completion-at-point-functions #'cape-file)
  (add-to-list 'completion-at-point-functions #'cape-dabbrev)
  (add-to-list 'completion-at-point-functions #'cape-keyword)
  (add-to-list 'completion-at-point-functions #'cape-symbol))

(summon! 'citar
  (global-set-key (kbd "C-c C-'") #'citar-insert-citation)
  (setq citar-file-extensions '("pdf" "epub"))
  (setq citar-bibliography '("~/org/bib/refs.bib" "~/org/bib/references.bib"))
  (setq citar-library-paths '("~/org/bib/pdfs")))

(summon! 'code-cells
  ;; depend on jupytext
  (with-eval-after-load "code-cells"
    (let ((map code-cells-mode-map))
      (define-key map (kbd "M-p") #'code-cells-backward-cell)
      (define-key map (kbd "M-n") #'code-cells-forward-cell)
      (define-key map (kbd "C-c C-c") #'code-cells-eval)
      (define-key map "n" (code-cells-speed-key #'code-cells-forward-cell))
      (define-key map "p" (code-cells-speed-key #'code-cells-backward-cell))
      (define-key map "e" (code-cells-speed-key #'code-cells-eval))
      (define-key map (kbd "TAB") (code-cells-speed-key (lambda ()
                                                          "Show/hide current cell"
                                                          (interactive)
                                                          (outline-minor-mode)
                                                          (if (outline-invisible-p (line-end-position))
                                                              (outline-show-subtree)
                                                            (outline-hide-subtree))))))
    (add-to-list 'code-cells-eval-region-commands '(julia-snail-mode . julia-snail-send-code-cell))))

(summon! 'comint-mime
  (add-hook 'shell-mode-hook #'comint-mime-setup)
  (add-hook 'inferior-python-mode-hook #'comint-mime-setup))

(summon! 'csv-mode
  (add-to-list 'auto-mode-alist '("\\.csv\\'" . csv-mode)))

(summon! 'darkroom
  (global-set-key (kbd "C-, ow") #'darkroom-tentative-mode))

(summon! 'diredfl
  (with-eval-after-load "dired"
    (diredfl-global-mode 1)))

(summon! 'easy-kill
  (global-set-key [remap kill-ring-save] #'easy-kill)
  (global-set-key (kbd "s-m") #'easy-mark))

(summon! 'elfeed
  (global-set-key (kbd "C-, mf") #'elfeed)
  (with-eval-after-load "elfeed"
    (define-key elfeed-search-mode-map (kbd "g") #'elfeed-update)
    (defun my/feed-font-setup ()
      (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
			       :height 1.3))
    (add-hook 'elfeed-show-mode-hook #'my/feed-font-setup)))

(summon! 'elfeed-org
  (setq rmh-elfeed-org-files (list "~/.emacs.d/configs.org"))
  (with-eval-after-load "elfeed"
    (elfeed-org)))

(summon! 'embark
  (global-set-key (kbd "C-.") #'embark-act)
  (global-set-key (kbd "C-;") #'embark-dwim)
  (global-set-key (kbd "C-, fB") #'embark-bindings)
  (setq embark-mixed-indicator-delay 0.7)
  (with-eval-after-load "embark"
    (define-key minibuffer-local-map (kbd "s-b") #'embark-become)
    (define-key embark-url-map "c" #'browse-url-chromium)
    (defun my/chromium-browse-bookmark (bm)
      "Open bookmark in Chromium."
      (browse-url-chromium (bookmark-location bm)))
    (define-key embark-bookmark-map "c" #'my/chromium-browse-bookmark)))

(summon! 'embrace
  (global-set-key (kbd "s-e") #'embrace-commander)
  (add-hook 'org-mode-hook #'embrace-org-mode-hook))

;; ESS
(summon! 'ess
  (add-to-list 'auto-mode-alist '("\\.R\\'" . R-mode))
  (add-to-list 'display-buffer-alist
	       '("^\\*help\\[R\\]"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (window-width . 0.382)
		 (slot . 1)))
  (add-to-list 'display-buffer-alist
	       '("^\\*R"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (slot . -1)
		 (window-width . 0.382)))
  (add-to-list 'display-buffer-alist
	       '("^\\*R\sDired"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (window-width . 0.382)
		 (slot . 0)))
  (add-to-list 'display-buffer-alist
	       '("^\\*R\sData\sView"
		 (display-buffer-reuse-window display-buffer-at-bottom)
		 (window-width . 0.618)
		 (slot . 0))))

(summon! 'ess-view-data
  (with-eval-after-load "ess-r-mode"
    (define-key ess-r-mode-map "v" #'ess-view-data-print)))

(summon! 'fanyi
  (global-set-key (kbd "C-, df") #'fanyi-dwim)
  (add-to-list 'display-buffer-alist
	       '("^\\*fanyi"
		 (display-buffer-reuse-mode-window display-buffer-in-side-window)
		 (window-width . 0.25)
		 (side . left)
		 (slot . 1))))

(summon! 'goto-chg
  (global-set-key (kbd "s-o") #'goto-last-change)
  (global-set-key (kbd "s-i") #'goto-last-change-reverse))

(summon! 'go-translate
  (global-set-key (kbd "C-, dF") #'gts-do-translate)
  (setq gts-translate-list '(("de" "en")))
  (with-eval-after-load "go-translate"
    (setq gts-default-translator
	  (gts-translator
	   :picker (gts-prompt-picker)
	   :engines (list (gts-google-engine) (gts-bing-engine))
	   :render (gts-buffer-render)))))

(summon! 'helpful
  (global-set-key (kbd "C-h k") #'helpful-key)
  (global-set-key (kbd "C-h f") #'helpful-function)
  (global-set-key (kbd "C-h F") #'helpful-macro)
  (global-set-key (kbd "C-h v") #'helpful-variable))

(summon! 'highlight-indent-guides
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-responsive t)
  (add-hook 'python-mode-hook #'highlight-indent-guides-mode)
  (add-hook 'julia-mode-hook #'highlight-indent-guides-mode))

(summon! 'info-colors
  (add-hook 'Info-selection-hook #'info-colors-fontify-node))

(summon! 'julia-mode
  (add-to-list 'auto-mode-alist '("\\.jl\\'" . julia-mode))
  (add-hook 'julia-mode-hook #'subword-mode))

(summon! 'julia-snail
  (add-hook 'julia-mode-hook #'julia-snail-mode)
  (add-to-list 'display-buffer-alist
	       '("^\\*julia"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (slot . -1)
		 (window-width . 0.382)))
  (add-to-list 'display-buffer-alist
	       '("^\\*julia\\*\sdocumentation"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (slot . 0)
		 (window-width . 0.382)))
  (add-to-list 'display-buffer-alist
	       '("^\\*julia\\*\smm"
		 (display-buffer-reuse-window display-buffer-in-side-window)
		 (side . right)
		 (window-width . 0.382)
		 (slot . 1))))

(summon! 'marginalia
  (marginalia-mode 1))

(summon! 'markdown-mode
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\`README\\.md\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.Rmd\\'" . markdown-mode)))

(summon! 'mct
  (setq mct-live-update-delay 0.5)
  (setq mct-display-buffer-action
	'((display-buffer-reuse-window
           display-buffer-in-side-window)
          (side . bottom)
          (slot . 0)))
  (mct-region-mode 1)
  (mct-minibuffer-mode 1)
  ;; avy integration
  (require 'avy)
  (defun prot/mct-avy--choose (pt)
    "Choose completion at PT."
    (goto-char pt)
    (mct-choose-completion-exit))
  (defun prot/avy-completions-select ()
    "Choose completion and exit using Avy."
    (interactive)
    (when-let ((window (mct--get-completion-window)))
      (with-selected-window window
	(avy-with avy-completion
          (let ((avy-action 'prot/mct-avy--choose))
            (avy-process
             (save-excursion
               (let (completions)
		 (goto-char (mct--first-completion-point))
		 (while (not (eobp))
                   (push (point) completions)
                   (next-completion 1))
		 (nreverse completions)))))))))
  (dolist (map (list mct-minibuffer-local-completion-map
                     mct-minibuffer-completion-list-map
                     mct-region-completion-list-map
                     mct-region-buffer-map))
    (define-key map (kbd "s-j") #'prot/avy-completions-select)))

(summon! 'minions
  (minions-mode 1))

(summon! 'modus-themes
  (modus-themes-load-themes)
  (modus-themes-load-operandi))

(summon! 'notmuch
  (global-set-key (kbd "C-, me") #'notmuch)
  (setq notmuch-show-logo nil)
  (setq notmuch-search-oldest-first nil)
  ;; Sending email (SMTP)
  (setq message-cite-reply-position 'above)
  (setq mail-default-directory "~/Mail")
  (setq send-mail-function 'smtpmail-send-it)
  (setq user-mail-address "duzaichuan@hotmail.com")
  (setq smtpmail-default-smtp-server "smtp.office365.com")
  (setq smtpmail-smtp-server "smtp.office365.com")
  (setq smtpmail-smtp-service 587)
  (setq smtpmail-stream-type 'starttls))

(summon! 'nov
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode))
  (setq nov-text-width 80)
  (setq nov-save-place-file "~/.emacs.d/lisp-data/nov-places")
  (defun my/nov-font-setup ()
    (face-remap-add-relative 'variable-pitch :family "Liberation Serif"
			     :height 1.4))
  (add-hook 'nov-mode-hook #'my/nov-font-setup))

(summon! 'orderless
  (require 'orderless)
  (setq completion-styles '(orderless partial-completion)))

;; orgmode
(summon! 'org
  (add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
  (add-hook 'org-mode-hook #'auto-fill-mode)
  (add-hook 'org-mode-hook #'org-num-mode)
  (global-set-key (kbd "C-, oa") #'org-agenda)
  (global-set-key (kbd "C-, oc") #'org-capture)
  (global-set-key (kbd "C-, ls") #'org-store-link)
  (with-eval-after-load "org"
    (define-key org-mode-map (kbd "C-c C-y") nil)
    (define-key org-mode-map (kbd "C-c C-j") nil)
    (define-key org-mode-map (kbd "C-,") nil)
    (define-key org-mode-map (kbd "<tab>") #'org-cycle)
    (define-key org-mode-map (kbd "C-c b") #'org-babel-tangle)
    (define-key org-mode-map (kbd "C-c f") #'org-footnote-action)
    (define-key org-mode-map (kbd "C-c o") #'org-toggle-narrow-to-subtree)
    (define-key org-mode-map (kbd "C-c t") #'org-set-tags-command)
    (define-key org-mode-map (kbd "C-c i") #'org-toggle-inline-images)
    (defun flyspell-delete-all-overlays ()
      "Delete all the overlays used by flyspell."
      (interactive)
      (flyspell-delete-region-overlays (point-min) (point-max)))
    (define-key org-mode-map (kbd "C-c S") #'flyspell-delete-all-overlays)
    (setq org-ellipsis "..")
    (setq org-tags-column 0)
    (setq org-return-follows-link t)
    (setq org-hide-emphasis-markers t)
    (setq porg-imenu-depth 3)
    (setq org-adapt-indentation nil)
    (setq org-latex-listings 'minted)
    (setq org-log-done 'time)
    (setq org-refile-use-outline-path 'file)
    (setq org-outline-path-complete-in-steps nil)
    (setq org-refile-allow-creating-parent-nodes 'confirm)
    (setq truncate-lines nil)
    (setq org-preview-latex-image-directory "~/Pictures/LaTeXimages/")
    (setq org-image-actual-width (/ (display-pixel-width) 2))
    (setq org-confirm-babel-evaluate nil)
    (setq org-src-fontify-natively t)
    (setq org-highlight-latex-and-related '(latex entities))
    (setq org-footnote-auto-adjust t)
    (setq org-export-with-smart-quotes t)
    (setq org-latex-caption-above '(table image))
    (setq org-latex-compiler "lualatex")
    (setq org-latex-bib-compiler "biber")
    (setq org-latex-remove-logfiles t)
    (setq org-catch-invisible-edits 'show)
    (setq org-cycle-separator-lines 0)
    (setq org-use-speed-commands t)
    (setq org-cite-global-bibliography '("~/org/bib/refs.bib"
					 "~/org/bib/references.bib"))
    (setq org-tag-alist '(("TOC" . ?T)
			  ("ignore" . ?i)
			  ("noexport" . ?n)))
    ;; lualatex preview
    (setq org-latex-pdf-process '("latexmk -shell-escape -silent -f -lualatex %f"))
    (setq luamagick '(luamagick :programs ("lualatex" "convert")
				:description "pdf > png"
				:message "you need to install lualatex and imagemagick."
				:use-xcolor t
				:image-input-type "pdf"
				:image-output-type "png"
				:image-size-adjust (1.5 . 1.5)
				:latex-compiler ("lualatex -interaction nonstopmode -output-directory %o %f")
				:image-converter ("convert -density %D -trim -antialias %f -quality 100 %O")))
    (add-to-list 'org-preview-latex-process-alist luamagick)
    (setq org-preview-latex-default-process 'luamagick)
    (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))
    (add-to-list 'org-file-apps '("\\.pdf\\'" . emacs))
    (add-to-list 'org-file-apps '("\\.x?html?\\'" . emacs))
    (require 'org-tempo) ;; Easy Templates Expansion
    ;; Automatically turn on hl-line-mode inside org-mode tables
    (defun my/highlight-current-table-line ()
      (interactive)
      (if (org-at-table-p)
	  (hl-line-mode 1)
	(hl-line-mode -1)))
    (defun my/setup-table-highlighting ()
      (add-hook 'post-command-hook #'my/highlight-current-table-line nil t))
    (add-hook 'org-mode-hook #'my/setup-table-highlighting)
    (add-hook 'orgtbl-mode-hook #'my/setup-table-highlighting)
    ;; org-cite csl style
    (require 'oc-csl)
    (setq org-cite-export-processors '((latex csl)
				       (t basic)))))

(summon! 'org-noter
  (setq org-noter-always-create-frame nil)
  (setq org-noter-default-notes-file-names '("annotated_bib.org"))
  (setq org-noter-notes-search-path '("~/org/bib" "~/org")))

(summon! '(org-super-links
	   :host github :repo "toshism/org-super-links")
  (with-eval-after-load "org"
    (define-key org-mode-map (kbd "C-c ll") #'org-super-links-store-link)
    (define-key org-mode-map (kbd "C-c ls") #'org-super-links-link)
    (define-key org-mode-map (kbd "C-c li") #'org-super-links-insert-link)
    (define-key org-mode-map (kbd "C-c ld") #'org-super-links-delete-link)))

(summon! 'org-tree-slide
  (defalias 'os #'org-tree-slide-mode)
  (add-hook 'org-tree-slide-play-hook (lambda ()
					(darkroom-tentative-mode 1)
					(org-display-inline-images)
					(read-only-mode 1)))
  (add-hook 'org-tree-slide-stop-hook (lambda ()
					(darkroom-tentative-mode -1)
					(org-remove-inline-images)
					(read-only-mode -1))))

(summon! 'pinyinlib
  (require 'pinyinlib)
  (defun completion--regex-pinyin (str)
    (orderless-regexp (pinyinlib-build-regexp-string str)))
  (add-to-list 'orderless-matching-styles 'completion--regex-pinyin))

(summon! '(pretex :host gitlab :repo "duzaichuan/pretex")
  (add-hook 'org-mode-hook #'pretex-mode)
  (add-hook 'eww-mode-hook #'pretex-mode))

(summon! 'python
  (add-to-list 'auto-mode-alist '("/Pipfile\\'" . conf-mode))
  (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode)))

(summon! 'rime
  (require 'rime)
  (setq default-input-method "rime")
  (setq rime-show-candidate 'minibuffer)
  (setq rime-user-data-dir "~/.local/share/fcitx5/rime")
  (setq rime-disable-predicates
	'(rime-predicate-after-alphabet-char-p
	  rime-predicate-prog-in-code-p)))

(summon! 'screenshot
  (global-set-key (kbd "s-s") #'screenshot)
  (global-set-key (kbd "s-S") #'screenshot-take)
  (setq screenshot-default-scheme "local"))

(summon! 'sudo-edit
  (global-set-key (kbd "C-c C-r") #'sudo-edit))

(summon! 'telega
  (global-set-key (kbd "C-, mt") #'telega)
  (add-hook 'telega-msg-ignore-predicates #'telega-msg-from-blocked-sender-p)
  (setq telega-server-libs-prefix "/usr/")
  (setq telega-symbol-reply "← ")
  (setq telega-symbol-forward "⇑"))

(summon! 'tempel
  (global-set-key (kbd "s-=") #'tempel-expand)
  (global-set-key (kbd "s-/") #'tempel-insert)
  (setq tempel-file "~/.emacs.d/lisp-data/templates")
  (with-eval-after-load "tempel"
    (define-key tempel-map (kbd "s-,") #'tempel-previous)
    (define-key tempel-map (kbd "s-.") #'tempel-next)))

(summon! 'vterm
  (global-set-key (kbd "<f1>") #'vterm)
  (with-eval-after-load "vterm"
    (define-key vterm-mode-map (kbd "<f1>") #'bury-buffer)
    (setq vterm-always-compile-module t)
    (add-to-list 'display-buffer-alist
		 '("^\\*vterm"
		   (display-buffer-reuse-window display-buffer-in-side-window)
		   (side . bottom)
		   (slot . 0)
		   (window-height . 0.25)))))

(summon! 'which-key
  (setq which-key-idle-delay 0.5)
  (which-key-mode 1)
  (which-key-setup-side-window-bottom))

(summon! 'yaml-mode
  (add-to-list 'auto-mode-alist '("\\.yaml\\'" . yaml-mode)))

(summon! 'ytdious
  (global-set-key (kbd "C-, my") #'ytdious)
  (setq ytdious-invidious-api-url "https://invidio.xamh.de/")
  (setq ytdious-player-external-options ""))

(summon! 'zk
  (global-set-key (kbd "C-, zn") #'zk-new-note)
  (global-set-key (kbd "C-, zf") #'zk-find-file)
  (global-set-key (kbd "C-, zi") #'zk-insert-link)
  (global-set-key (kbd "C-, zR") #'zk-rename-note)
  (global-set-key (kbd "C-, zs") #'zk-search)
  (setq zk-directory "~/zk")
  (setq zk-file-extension "md")
  (with-eval-after-load "zk"
    (zk-setup-auto-link-buttons)
    (zk-setup-embark)
    ;; fix org links
    (defun zk-org-try-to-follow-link (fn &optional arg)
      "When 'org-open-at-point' FN fails, try 'zk-follow-link-at-point'.
Optional ARG."
      (let ((org-link-search-must-match-exact-headline t))
	(condition-case nil
	    (apply fn arg)
	  (error (zk-follow-link-at-point)))))
    (advice-add 'org-open-at-point :around #'zk-org-try-to-follow-link)))
